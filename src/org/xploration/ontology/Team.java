package org.xploration.ontology;


import jade.content.*;
import jade.util.leap.*;
import jade.core.*;

/**
* Protege name: Team
* @author ontology bean generator
* @version 2017/04/24, 19:14:20
*/
public class Team implements Concept {

   /**
* Protege name: teamId
   */
   private int teamId;
   public void setTeamId(int value) { 
    this.teamId=value;
   }
   public int getTeamId() {
     return this.teamId;
   }

}
